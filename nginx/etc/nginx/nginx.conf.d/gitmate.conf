server {
    listen 443 ssl;
    server_name your.domain.tld;

    ssl_certificate /etc/ssl/ssl.crt;
    ssl_certificate_key /etc/ssl/ssl.key;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers HIGH:!aNULL:!MD5;

    client_max_body_size 4G;

    keepalive_timeout 5;

    location @proxy_to_backend {
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto https;
        proxy_set_header Host $http_host;
        proxy_redirect off;
        proxy_pass http://backend:8000;
    }

    location ~ ^/(admin|auth|api|docs|webhooks|logout|coala_online)(.*)$ {
        try_files $uri @proxy_to_backend;
    }

    location /static {
        alias /usr/src/app/static;
        autoindex on;
    }

    root /usr/src/app/frontend/dist;
    try_files $uri /index.html;
}
